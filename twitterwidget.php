<?php
/**
 * PrestaShop module created by VEKIA, a guy from official PrestaShop community ;-)
 *
 * @author    VEKIA https://www.prestashop.com/forums/user/132608-vekia/
 * @copyright 2010-2018 VEKIA
 * @license   This program is not free software and you can't resell and redistribute it
 *
 * CONTACT WITH DEVELOPER http://mypresta.eu
 * support@mypresta.eu
 */

class twitterwidget extends Module
{
    function __construct()
    {
        $this->name          = 'twitterwidget';
        $this->tab           = 'social_networks';
        $this->version       = '1.3.1';
        $this->author        = 'MyPresta.eu';
        $this->dir           = '/modules/twitterwidget/';
        $this->mypresta_link = 'https://mypresta.eu/modules/social-networks/twitter-widget-free.html';
        $this->bootstrap     = true;
        parent::__construct();
        $this->displayName = $this->l('Twitter Widget Free');
        $this->description = $this->l('This module adds a twitter widget to your shop.');
        $this->mkey        = "freelicense";
    }

    public static function psversion($part = 1)
    {
        $version = _PS_VERSION_;
        $exp     = $explode = explode(".", $version);
        if ($part == 1) {
            return $exp[1];
        }
        if ($part == 2) {
            return $exp[2];
        }
        if ($part == 3) {
            return $exp[3];
        }
    }

    public function inconsistency($ret = 0)
    {
        return;
    }

    public function checkforupdates($display_msg = 0, $form = 0)
    {
        // ---------- //
        // ---------- //
        // VERSION 12 //
        // ---------- //
        // ---------- //
        if (@file_exists('../modules/' . $this->name . '/key.php')) {
            @require_once('../modules/' . $this->name . '/key.php');
        } else {
            if (@file_exists(dirname(__FILE__) . $this->name . '/key.php')) {
                @require_once(dirname(__FILE__) . $this->name . '/key.php');
            } else {
                if (@file_exists('modules/' . $this->name . '/key.php')) {
                    @require_once('modules/' . $this->name . '/key.php');
                }
            }
        }
        if ($form == 1) {
            return '
            <div class="panel" id="fieldset_myprestaupdates" style="margin-top:20px;">
            ' . ($this->psversion() == 6 || $this->psversion() == 7 ? '<div class="panel-heading"><i class="icon-wrench"></i> ' . $this->l('MyPresta updates') . '</div>' : '') . '
			<div class="form-wrapper" style="padding:0px!important;">
            <div id="module_block_settings">
                    <fieldset id="fieldset_module_block_settings">
                         ' . ($this->psversion() == 5 ? '<legend style="">' . $this->l('MyPresta updates') . '</legend>' : '') . '
                        <form action="' . $_SERVER['REQUEST_URI'] . '" method="post">
                            <label>' . $this->l('Check updates') . '</label>
                            <div class="margin-form">' . (Tools::isSubmit('submit_settings_updates_now') ? ($this->inconsistency(0) ? '' : '') . $this->checkforupdates(1) : '') . '
                                <button style="margin: 0px; top: -3px; position: relative;" type="submit" name="submit_settings_updates_now" class="button btn btn-default" />
                                <i class="process-icon-update"></i>
                                ' . $this->l('Check now') . '
                                </button>
                            </div>
                            <label>' . $this->l('Updates notifications') . '</label>
                            <div class="margin-form">
                                <select name="mypresta_updates">
                                    <option value="-">' . $this->l('-- select --') . '</option>
                                    <option value="1" ' . ((int)(Configuration::get('mypresta_updates') == 1) ? 'selected="selected"' : '') . '>' . $this->l('Enable') . '</option>
                                    <option value="0" ' . ((int)(Configuration::get('mypresta_updates') == 0) ? 'selected="selected"' : '') . '>' . $this->l('Disable') . '</option>
                                </select>
                                <p class="clear">' . $this->l('Turn this option on if you want to check MyPresta.eu for module updates automatically. This option will display notification about new versions of this addon.') . '</p>
                            </div>
                            <label>' . $this->l('Module page') . '</label>
                            <div class="margin-form">
                                <a style="font-size:14px;" href="' . $this->mypresta_link . '" target="_blank">' . $this->displayName . '</a>
                                <p class="clear">' . $this->l('This is direct link to official addon page, where you can read about changes in the module (changelog)') . '</p>
                            </div>
                            <div class="panel-footer">
                                <button type="submit" name="submit_settings_updates"class="button btn btn-default pull-right" />
                                <i class="process-icon-save"></i>
                                ' . $this->l('Save') . '
                                </button>
                            </div>
                        </form>
                    </fieldset>
                    <style>
                    #fieldset_myprestaupdates {
                        display:block;clear:both;
                        float:inherit!important;
                    }
                    </style>
                </div>
            </div>
            </div>';
        } else {
            if (defined('_PS_ADMIN_DIR_')) {
                if (Tools::isSubmit('submit_settings_updates')) {
                    Configuration::updateValue('mypresta_updates', Tools::getValue('mypresta_updates'));
                }
                if (Configuration::get('mypresta_updates') != 0 || (bool)Configuration::get('mypresta_updates') == false) {
                    if (Configuration::get('update_' . $this->name) < (date("U") - 259200)) {
                        $actual_version = twitterwidgetUpdate::verify($this->name, (isset($this->mkey) ? $this->mkey : 'nokey'), $this->version);
                    }
                    if (twitterwidgetUpdate::version($this->version) < twitterwidgetUpdate::version(Configuration::get('updatev_' . $this->name))) {
                        $this->warning = $this->l('New version available, check http://MyPresta.eu for more informations');
                    }
                }
                if ($display_msg == 1) {
                    if (twitterwidgetUpdate::version($this->version) < twitterwidgetUpdate::version(twitterwidgetUpdate::verify($this->name, (isset($this->mkey) ? $this->mkey : 'nokey'), $this->version))) {
                        return "<span style='color:red; font-weight:bold; font-size:16px; margin-right:10px;'>" . $this->l('New version available!') . "</span>";
                    } else {
                        return "<span style='color:green; font-weight:bold; font-size:16px; margin-right:10px;'>" . $this->l('Module is up to date!') . "</span>";
                    }
                }
            }
        }
    }


    function install()
    {
        if (parent::install() == false OR ! Configuration::updateValue('update_' . $this->name, '0') OR $this->registerHook('rightColumn') == false OR $this->registerHook('header') == false OR $this->registerHook('leftColumn') == false OR $this->registerHook('home') == false OR $this->registerHook('footer') == false OR Configuration::updateValue('twitterwidget_position', '2') == false OR Configuration::updateValue('twitterwidget_name', 'myprestaeu') == false OR Configuration::updateValue('twitterwidget_width', '220') == false OR Configuration::updateValue('twitterwidget_height', '420') == false) {
            return false;
        }

        return true;
    }

    public function getContent()
    {
        $output = "";
        if (Tools::isSubmit('submit_settings')) {
            Configuration::updateValue('twitterwidget_position', Tools::getValue('new_twitterwidget_position'), true);
            Configuration::updateValue('twitterwidget_name', Tools::getValue('new_twitterwidget_name'), true);
            Configuration::updateValue('twitterwidget_width', Tools::getValue('new_twitterwidget_width'), true);
            Configuration::updateValue('twitterwidget_height', Tools::getValue('new_twitterwidget_height'), true);
            Configuration::updateValue('twitterwidget_color', Tools::getValue('new_twitterwidget_color'), true);
            Configuration::updateValue('twitterwidget_heading', Tools::getValue('new_twitterwidget_heading'), true);

            $this->context->controller->confirmations[] = $this->l('Settings properly saved');
        }

        return $this->displayForm() . $this->checkforupdates(0, 1);
    }

    public function getconf()
    {
        $var                         = new stdClass();
        $var->twitterwidget_position = Configuration::get('twitterwidget_position');
        $var->twitterwidget_name     = Configuration::get('twitterwidget_name');
        $var->twitterwidget_width    = Configuration::get('twitterwidget_width');
        $var->twitterwidget_height   = Configuration::get('twitterwidget_height');
        $var->twitterwidget_color    = Configuration::get('twitterwidget_color');
        $var->twitterwidget_heading  = Configuration::get('twitterwidget_heading');

        return $var;
    }

    public function displayForm()
    {
        $var           = $this->getconf();
        $twt_position1 = "";
        $twt_position2 = "";
        $twt_position3 = "";
        $twt_position4 = "";
        if ($var->twitterwidget_position == 1) {
            $twt_position1 = "checked=\"yes\"";
        }
        if ($var->twitterwidget_position == 2) {
            $twt_position2 = "checked=\"yes\"";
        }
        if ($var->twitterwidget_position == 3) {
            $twt_position3 = "checked=\"yes\"";
        }
        if ($var->twitterwidget_position == 4) {
            $twt_position4 = "checked=\"yes\"";
        }

        return '
        <style>
        .margin-form input, .margin-form select {max-width:200px;}
        </style>
        <div class="panel">
        <iframe src="//mypresta.eu/content/uploads/2012/10/facebook_advertise.html" width="100%" height="130" border="0" style="border:none;"></iframe>
        </div>
		<form action="' . $_SERVER['REQUEST_URI'] . '" method="post">
            <div style="display:block; margin:auto; overflow:hidden; ">
                    <div style="clear:both; display:block; " class="panel">
                        <h3>' . $this->l('Twitter Widget configuration') . '</h3>
                        <div class="nobootstrap" style="padding:0px!important">
            				

			                <div style="clear:both;display:block;">
							    <label>' . $this->l('Left column') . ':</label>
								<div class="margin-form" valign="middle">
			                        <div style="margin-top:7px;">
									<input type="radio" name="new_twitterwidget_position" value="1" ' . $twt_position1 . '> ' . $this->l('yes') . '	
			                        </div>
								</div>
			                </div>
			                <div style="clear:both;display:block;">
							    <label>' . $this->l('Right column') . ':</label>
								<div class="margin-form" valign="middle">
			                        <div style="margin-top:7px;">
									<input type="radio" name="new_twitterwidget_position" value="2" ' . $twt_position2 . '> ' . $this->l('yes') . '									
			                        </div>
								</div>
			                </div>
							<div style="clear:both;display:block;">
							    <label>' . $this->l('Home') . ':</label>
								<div class="margin-form" valign="middle">
			                        <div style="margin-top:7px;">
									<input type="radio" name="new_twitterwidget_position" value="3" ' . $twt_position3 . '> ' . $this->l('yes') . '
									
			                        </div>
								</div>
			                </div>
                            <div style="clear:both;display:block;">
							    <label>' . $this->l('Footer') . ':</label>
								<div class="margin-form" valign="middle">
			                        <div style="margin-top:7px;">
									<input type="radio" name="new_twitterwidget_position" value="4" ' . $twt_position4 . '> ' . $this->l('yes') . '
									
			                        </div>
								</div>
			                </div>
			                
			                <label>' . $this->l('Box width') . '</label>
            					<div class="margin-form">
            						<input type="text" style="width:200px;" value="' . $var->twitterwidget_width . '" id="new_twitterwidget_width" name="new_twitterwidget_width" onchange="">
                                </div>
                                
                            <label>' . $this->l('Box height') . '</label>
            					<div class="margin-form">
            						<input type="text" style="width:200px;" value="' . $var->twitterwidget_height . '" id="new_twitterwidget_height" name="new_twitterwidget_height" onchange="">
                                </div>
                                
                            <label>' . $this->l('Box colors') . '</label>
            					<div class="margin-form">
            						<select id="new_twitterwidget_color" name="new_twitterwidget_color" onchange="">
                                        <option ' . ($var->twitterwidget_color == 'light' ? 'selected="yes"' : '') . ' value="light">' . $this->l('Light colors') . '</option>
                                        <option ' . ($var->twitterwidget_color == 'dark' ? 'selected="yes"' : '') . ' value="dark">' . $this->l('Dark colors') . '</option>
            						</select>
                                </div>
                                
                            <label>' . $this->l('Heading') . '</label>
            					<div class="margin-form">
            						<select id="new_twitterwidget_heading" name="new_twitterwidget_heading" onchange="">
                                        <option ' . ($var->twitterwidget_heading == '1' ? 'selected="yes"' : '') . ' value="1">' . $this->l('Yes') . '</option>
                                        <option ' . ($var->twitterwidget_heading != '1' ? 'selected="yes"' : '') . ' value="0">' . $this->l('No') . '</option>
            						</select>
            						<p class="description">' . $this->l('This option when enabled will create a box with heading "Twitter"') . '</p>
                                </div>                                
				                            
            				<label>' . $this->l('Twitter name') . '</label>
            					<div class="margin-form">
            						<input type="text" style="width:400px;" value="' . $var->twitterwidget_name . '" id="new_twitterwidget_name" name="new_twitterwidget_name" onchange="">
                                    <p class="clear">' . $this->l('Your account on Twitter name') . '</p>
                                </div>
                                
                                <input name="submit_settings" type="hidden">
                        </div>    
                        <div class="panel-footer">
                        <button class="button btn btn-default pull-right"><i class="process-icon-save"></i>' . $this->l('Save') . '</button>
                        </div>                
                    </div>
            </div>
		</form>
        ';
    }


    function hookrightColumn($params)
    {
        if (Configuration::get('twitterwidget_position') == 2) {
            $cfg = $this->getconf();
            global $smarty;
            $smarty->assign('twt', $cfg);

            return $this->display(__FILE__, 'rightcolumn.tpl');
        }
    }

    function hookleftColumn($params)
    {
        if (Configuration::get('twitterwidget_position') == 1) {
            $cfg = $this->getconf();
            global $smarty;
            $smarty->assign('twt', $cfg);

            return $this->display(__FILE__, 'rightcolumn.tpl');
        }
    }

    function hookhome($params)
    {
        if (Configuration::get('twitterwidget_position') == 3) {
            $cfg = $this->getconf();
            global $smarty;
            $smarty->assign('twt', $cfg);

            return $this->display(__FILE__, 'rightcolumn.tpl');
        }
    }

    public function hookHeader($params)
    {
        $this->context->controller->addCSS($this->_path . 'twitterwidget.css');
    }

    function hookFooter($params)
    {
        if (Configuration::get('twitterwidget_position') == 4) {
            $cfg = $this->getconf();
            global $smarty;
            $smarty->assign('twt', $cfg);

            return $this->display(__FILE__, 'rightcolumn.tpl');
        }
    }
}

class twitterwidgetUpdate extends twitterwidget
{
    public static function version($version)
    {
        $version = (int)str_replace(".", "", $version);
        if (strlen($version) == 3) {
            $version = (int)$version . "0";
        }
        if (strlen($version) == 2) {
            $version = (int)$version . "00";
        }
        if (strlen($version) == 1) {
            $version = (int)$version . "000";
        }
        if (strlen($version) == 0) {
            $version = (int)$version . "0000";
        }

        return (int)$version;
    }

    public static function encrypt($string)
    {
        return base64_encode($string);
    }

    public static function verify($module, $key, $version)
    {
        if (ini_get("allow_url_fopen")) {
            if (function_exists("file_get_contents")) {
                $actual_version = @file_get_contents('http://dev.mypresta.eu/update/get.php?module=' . $module . "&version=" . self::encrypt($version) . "&lic=$key&u=" . self::encrypt(_PS_BASE_URL_ . __PS_BASE_URI__));
            }
        }
        Configuration::updateValue("update_" . $module, date("U"));
        Configuration::updateValue("updatev_" . $module, $actual_version);

        return $actual_version;
    }
}

?>