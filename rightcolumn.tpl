{*
* PrestaShop module created by VEKIA, a guy from official PrestaShop community ;-)
*
* @author    VEKIA https://www.prestashop.com/forums/user/132608-vekia/
* @copyright 2010-2018 VEKIA
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER http://mypresta.eu
* support@mypresta.eu
*}

{if $twt->twitterwidget_heading == 1}<div class="twitterwidget_feed block"><p class="title_block">{l s='Twitter' mod='twitterwidget'}</p>{/if}
<a class="twitter-timeline" data-theme="{$twt->twitterwidget_color}" data-width="{$twt->twitterwidget_width}" data-height="{$twt->twitterwidget_height}" href="https://twitter.com/{$twt->twitterwidget_name}?ref_src=twsrc%5Etfw"></a>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
{if $twt->twitterwidget_heading == 1}</div>{/if}